import java.lang.reflect.Array;
import java.util.Arrays;

public class PKCS5 {
    static final int k = 16;

    byte[] pad(byte[] data) {
        int p = k - (data.length % k);
        byte[] padded = new byte[data.length + p];
        System.arraycopy(data, 0, padded, 0, data.length);
        Arrays.fill(padded, data.length, padded.length, (byte) p);
        return padded;
    }

    byte[] unpad(byte[] data) {
        int p = data[data.length - 1];
        int l = data.length - p;
        return Arrays.copyOf(data, l);
    }
}
