import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class pkcs5 {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Error : take one path as argument");
            return;
        }

        String fileName = args[0];
        Path filePath = Paths.get(fileName);
        byte[] data = Files.readAllBytes(filePath);
        String newFileName = filePath.getParent().toString() + "/pkcs5-" + filePath.getFileName().toString();
        Files.write(Paths.get(newFileName), (new PKCS5()).pad(data));
    }
}
