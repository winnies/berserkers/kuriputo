import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class inv_pkcs5 {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Error : take one path as argument");
            return;
        }

        String fileName = args[0];
        byte[] data = Files.readAllBytes(Paths.get(fileName));
        Files.write(Paths.get(fileName), (new PKCS5()).unpad(data));
    }
}
