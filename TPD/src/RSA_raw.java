// -*- coding: utf-8 -*-

import java.math.BigInteger;
import java.util.Random;

public class RSA_raw {

    private static BigInteger code, codeChiffré, codeDéchiffré ;
    private static BigInteger p ;
    private static BigInteger q ;
    private static BigInteger w ;
    private static BigInteger n ;      // Le module de la clef publique
    private static BigInteger e ;      // L'exposant de la clef publique
    private static BigInteger d ;      // L'exposant de la clef privée

    static Random alea = new Random();

    static void fabrique() {           // Fabrique d'une paire de clefs RSA (A MODIFIER)
        p = grand_nombre_aleatoire_premier(1024);
        q = grand_nombre_aleatoire_premier(1024);
        n = p.multiply(q);
        w = q.subtract(BigInteger.ONE).multiply(p.subtract(BigInteger.ONE));
        //n = new BigInteger("196520034100071057065009920573", 10);
        //e = new BigInteger("7", 10);
        d = retourner_d_inversible(w);
        e = d.modInverse(w);
        //d = new BigInteger("56148581171448620129544540223", 10);
    }

    static boolean est_probablement_premier(BigInteger n)
    {
        return n.isProbablePrime(50);
    }

    static BigInteger grand_nombre_aleatoire_premier(int nb_bits) {
        BigInteger x;
        do {
            x = new BigInteger(nb_bits, alea);
            x = x.setBit(0);
            x = x.setBit(nb_bits - 1);
        } while (!est_probablement_premier(x));
        return x;
    }

    static BigInteger retourner_d_inversible(BigInteger w) {
        BigInteger d_temp;
        do {
            d_temp = new BigInteger(w.bitCount(), alea).add(BigInteger.ONE).mod(w);
        } while(!d_temp.gcd(w).equals(BigInteger.ONE));
        return d_temp;
    }

    public static void main(String[] args) {
        code = new BigInteger("4b594f544f", 16);

        /* Affichage du code clair */
        System.out.println("Code clair        : " + code);

        fabrique();

        /* Affichage des clefs utilisées */
        System.out.println("Clef publique (n) : " + n);
        System.out.println("Clef publique (e) : " + e);
        System.out.println("Clef privée (d)   : " + d);

        /* On effectue d'abord le chiffrement RSA du code clair avec la clef publique */
        codeChiffré = code.modPow(e, n);
        System.out.println("Code chiffré      : " + codeChiffré);

        /* On déchiffre ensuite avec la clef privée */
        codeDéchiffré = codeChiffré.modPow(d, n);
        System.out.println("Code déchiffré    : " + codeDéchiffré);
    }
}