// -*- coding: utf-8 -*-

import java.math.BigInteger;
import java.util.Random;

public class EPP
{
    static Random alea = new Random();

    static double nbCumuleTentatives = 0;
    static double tempsCumuleExecution = 0;

    public static void main(String[] args)
    {
        for(int i = 0; i < 10; i++) {
            BigInteger n = grand_nombre_aleatoire_premier(); //new BigInteger("170141183460469231731687303715884105727", 10);
        }
      /*  System.out.print("Le nombre " + n);
        if (est_probablement_premier(n))
            System.out.println(" est très probablement premier!");
        else
            System.out.println(" n'est absolument pas premier!");*/

        System.out.println("Nombre moyen de tentatives : " + nbCumuleTentatives / 10);
        System.out.println("Temps moyen de calcul : " + tempsCumuleExecution / 10 + " ms");
    }

    static BigInteger grand_nombre_aleatoire_premier() {
        BigInteger x;
        long debut = System.currentTimeMillis();
        int nbTentatives = 0;
        long tempsExecution = 0;
        do {
            x = new BigInteger(512, alea);
            x = x.setBit(0);
            x = x.setBit(511);
            nbTentatives ++;
        } while (!est_probablement_premier(x));
        tempsExecution = System.currentTimeMillis()-debut;
        System.out.println("Temps d'exécution : " + tempsExecution +  " ms");
        System.out.println("Nombre de tentatives : " + nbTentatives);
        System.out.println ("Valeur de x : " + x);
        nbCumuleTentatives += nbTentatives;
        tempsCumuleExecution += tempsExecution;
        return x;
    }

    static boolean est_probablement_premier(BigInteger n)
    {
        /*
          Modifiez cette fonction afin qu'elle retourne si oui
          ou non l'entier n est un nombre premier, avec un taux
          d'erreur inférieur à 1/1 000 000 000 000 000.
          Valeur choisie : 50 car 2⁵⁰ = 1 125 899 906 842 624
        */
        return n.isProbablePrime(50);
    }
}
