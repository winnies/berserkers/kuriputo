import java.io.*;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;

public class Exo1 {

    public static BigInteger e = new BigInteger("44bb1ff6c2b674798e09075609b7883497ae2e2d7b06861ef9850e26d1456280523319021062c8743544877923fe65f85111792a98e4b887de8ffd13aef18ff7f6f736c821cfdad98af051e7caaa575d30b54ed9a6ee901bb0ffc17e25d444f8bfc5922325ee2ef94bd4ee15bede2ea12eb623ad507d6b246a1f0c3cc419f155", 16);
    public static BigInteger n = new BigInteger("94f28651e58a75781cfe69900174b86f855f092f09e3da2ad86b4ed964a84917e5ec60f4ee6e3adaa13962884e5cf8dae2e0d29c6168042ec9024ea11176a4ef031ac0f414918b7d13513ca1110ed80bd2532f8a7aab0314bf54fcaf621eda74263faf2a5921ffc515097a3c556bf86f2048a3c159fccfee6d916d38f7f23f21", 16);

    public static String toHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for(byte b : bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    // Arg 1: input, arg 2 : output
    public static void main(String[] args) throws Exception {

        SecureRandom alea = new SecureRandom();
        FileInputStream fis = new FileInputStream(args[0]);
        FileOutputStream fos = new FileOutputStream(args[1]);

        // Génération de la clef brute AES
        byte[] clefBrute = new byte[16];
        alea.nextBytes(clefBrute);
        System.out.println("Clef de session AES (brute) : 0x" + toHex(clefBrute));

        // Chiffrement RSA de la clef brute
        KeyFactory usineAClefs = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec specPub = new RSAPublicKeySpec(n, e);
        PublicKey clefPubliqueRSA = usineAClefs.generatePublic(specPub);
        Cipher chiffreurRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        chiffreurRSA.init(Cipher.ENCRYPT_MODE, clefPubliqueRSA);
        byte[] clefSessionAESChiffree = chiffreurRSA.doFinal(clefBrute);

        fos.write(clefSessionAESChiffree, 0, clefSessionAESChiffree.length);

        // Vecteur d'initialisation
        byte[] vecteurIni = new byte[16];
        alea.nextBytes(vecteurIni);
        System.out.println("Vecteur d'initialisation : 0x" + toHex(vecteurIni));

        fos.write(vecteurIni, 0, vecteurIni.length);

        // Chiffrement AES
        Cipher chiffreurAES = Cipher.getInstance("AES/CBC/PKCS5Padding");
        IvParameterSpec ivspec = new IvParameterSpec(vecteurIni);
        SecretKeySpec clefSessionAES = new SecretKeySpec(clefBrute, "AES");
        chiffreurAES.init(Cipher.ENCRYPT_MODE, clefSessionAES, ivspec);
        CipherInputStream cis = new CipherInputStream(fis, chiffreurAES);

        int nbOctetsLus;
        byte[] buffer = new byte[256];
        while((nbOctetsLus = cis.read(buffer)) != -1) {
            fos.write(buffer, 0, nbOctetsLus);
        }

        fis.close();
        fos.close();
        cis.close();
    }
}
