// -*- coding: utf-8 -*-

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class AES {

    public AES() {
        init_Inv_SBox();
    }

    public byte K[] = new byte[32];   // Une clef courte a une longueur maximale de 32 octets
    public byte W[] = new byte[240];  // La longueur maximale de W est (14+1)*16=240 octets

    int Nr;
    int Nk;
    int longueur_de_la_clef;
    int longueur_de_la_clef_etendue;

    public static byte[] Rcon = { (byte) 0x01, (byte) 0x02, (byte) 0x04, (byte) 0x08, (byte) 0x10, (byte) 0x20, (byte) 0x40, (byte) 0x80, (byte) 0x1b, (byte) 0x36 } ;

    public byte State[] = new byte[16];

    public void affiche_la_clef(byte clef[], int longueur) {
        for(int i = 0; i < 4; i ++) {
            for(int j = 0; j < longueur / 4; j++) {
                System.out.printf("%02X", clef[j*4 + i]);
                if((j+1)%4 == 0) System.out.print(" ");
            }
            System.out.println();
        }
    }

    public void calcule_la_clef_etendue() { // À modifier !
        if (longueur_de_la_clef == 16) {
            Nr = 10; Nk = 4;
        } else if (longueur_de_la_clef == 24) {
            Nr = 12; Nk = 6;
        } else {
            Nr = 14; Nk = 8;
        }
        longueur_de_la_clef_etendue = 4 * (4 * (Nr + 1));
        for (int i = 0; i < longueur_de_la_clef_etendue; i++) {
            W[i] = 0;
        }

        for (int i = 0; i < longueur_de_la_clef; ++i) {
            W[i] = K[i];
        }

        byte[] tmp = new byte[4];
        byte[] rcon = {(byte) 0, (byte) 0, (byte) 0, (byte) 0};
        for(int i = Nk; i < 4* (Nr + 1); ++i) {
            System.arraycopy(W, (i-1) * 4, tmp, 0, 4);
            if(i % Nk == 0) {
                rotWord(tmp);
                subWord(tmp);
                rcon[0] = Rcon[i/Nk - 1];
                tmp = XOR(tmp, rcon);
            }
            else if(Nk > 6 && i % Nk == 4) {
                subWord(tmp);
            }
            tmp = XOR(Arrays.copyOfRange(W, (i - Nk) * 4, (i - Nk) * 4 + 4), tmp);
            System.arraycopy(tmp, 0, W, i * 4, 4);
        }

    }

    private void rotWord(byte[] col) {
        assert(col.length > 0);

        byte first = col[0];
        for (int i = 0; i < col.length - 1; ++i) {
            col[i] = col[i + 1];
        }
        col[col.length - 1] = first;
    }

    private void subWord(byte[] col) {
        for(int i = 0; i < col.length; i++){
            col[i] = SBox[Byte.toUnsignedInt(col[i])];
        }
    }

    public void encrypt(FileInputStream fis, FileOutputStream fos, byte[] clefBrute, byte[] IV) throws IOException {
        assert (clefBrute.length==16) || (clefBrute.length==24) || (clefBrute.length == 32);
        longueur_de_la_clef = clefBrute.length;

        PKCS5 pkcs5 = new PKCS5();

        Arrays.fill(K, (byte) 0);
        System.arraycopy(clefBrute, 0, K, 0, clefBrute.length);
        calcule_la_clef_etendue();

        System.out.println("Clef courte :");
        affiche_la_clef(K, longueur_de_la_clef);
        System.out.println();
        System.out.println("Clef longue :");
        affiche_la_clef(W, longueur_de_la_clef_etendue);
        System.out.println();

        int nbOctetsLus;
        while((nbOctetsLus = fis.read(State)) != -1) {
            System.out.println("1) Bloc lu : \n");
            afficher_le_bloc(State) ;
            if(nbOctetsLus != 16) {
                System.out.println("\nPADDING : (bloc de " + nbOctetsLus + " octets)");
                State = pkcs5.pad(Arrays.copyOf(State, nbOctetsLus));
            }
            System.out.println();

            System.out.println("2) XOR : \n");
            afficher_le_bloc(State) ;
            System.out.println(" + ");
            afficher_le_bloc(IV);
            State = XOR(State, IV);
            System.out.println( " = ");
            afficher_le_bloc(State) ;
            System.out.println();

            System.out.println("3) Bloc chiffré : \n");
            chiffrer();
            afficher_le_bloc(State) ;
            System.out.println();

            fos.write(State, 0, 16);
            System.arraycopy(State, 0, IV, 0, 16);
        }
    }

    public void afficher_le_bloc(byte M[]) {
        for (int i=0; i<4; i++) { // Lignes 0 à 3
            System.out.print("          ");
            for (int j=0; j<4; j++) { // Colonnes 0 à 3
                System.out.print(String.format("%02X ", M[4*j+i]));
            }
            System.out.println();
        }
    }

    public void chiffrer(){
        AddRoundKey(0);
        for (int i = 1; i < Nr; i++) {
            SubBytes();
            ShiftRows();
            MixColumns();
            AddRoundKey(i);
        }
        SubBytes();
        ShiftRows();
        AddRoundKey(Nr);
    }

    public void dechiffrer(){
        AddRoundKey(Nr);
        Inv_ShiftRows();
        Inv_SubBytes();
        for (int i = Nr-1; i > 0; i--) {
            AddRoundKey(i);
            Inv_MixColumns();
            Inv_ShiftRows();
            Inv_SubBytes();
        }
        AddRoundKey(0);
    }

    public byte[] SBox = {
            (byte)0x63, (byte)0x7C, (byte)0x77, (byte)0x7B, (byte)0xF2, (byte)0x6B, (byte)0x6F, (byte)0xC5,
            (byte)0x30, (byte)0x01, (byte)0x67, (byte)0x2B, (byte)0xFE, (byte)0xD7, (byte)0xAB, (byte)0x76,
            (byte)0xCA, (byte)0x82, (byte)0xC9, (byte)0x7D, (byte)0xFA, (byte)0x59, (byte)0x47, (byte)0xF0,
            (byte)0xAD, (byte)0xD4, (byte)0xA2, (byte)0xAF, (byte)0x9C, (byte)0xA4, (byte)0x72, (byte)0xC0,
            (byte)0xB7, (byte)0xFD, (byte)0x93, (byte)0x26, (byte)0x36, (byte)0x3F, (byte)0xF7, (byte)0xCC,
            (byte)0x34, (byte)0xA5, (byte)0xE5, (byte)0xF1, (byte)0x71, (byte)0xD8, (byte)0x31, (byte)0x15,
            (byte)0x04, (byte)0xC7, (byte)0x23, (byte)0xC3, (byte)0x18, (byte)0x96, (byte)0x05, (byte)0x9A,
            (byte)0x07, (byte)0x12, (byte)0x80, (byte)0xE2, (byte)0xEB, (byte)0x27, (byte)0xB2, (byte)0x75,
            (byte)0x09, (byte)0x83, (byte)0x2C, (byte)0x1A, (byte)0x1B, (byte)0x6E, (byte)0x5A, (byte)0xA0,
            (byte)0x52, (byte)0x3B, (byte)0xD6, (byte)0xB3, (byte)0x29, (byte)0xE3, (byte)0x2F, (byte)0x84,
            (byte)0x53, (byte)0xD1, (byte)0x00, (byte)0xED, (byte)0x20, (byte)0xFC, (byte)0xB1, (byte)0x5B,
            (byte)0x6A, (byte)0xCB, (byte)0xBE, (byte)0x39, (byte)0x4A, (byte)0x4C, (byte)0x58, (byte)0xCF,
            (byte)0xD0, (byte)0xEF, (byte)0xAA, (byte)0xFB, (byte)0x43, (byte)0x4D, (byte)0x33, (byte)0x85,
            (byte)0x45, (byte)0xF9, (byte)0x02, (byte)0x7F, (byte)0x50, (byte)0x3C, (byte)0x9F, (byte)0xA8,
            (byte)0x51, (byte)0xA3, (byte)0x40, (byte)0x8F, (byte)0x92, (byte)0x9D, (byte)0x38, (byte)0xF5,
            (byte)0xBC, (byte)0xB6, (byte)0xDA, (byte)0x21, (byte)0x10, (byte)0xFF, (byte)0xF3, (byte)0xD2,
            (byte)0xCD, (byte)0x0C, (byte)0x13, (byte)0xEC, (byte)0x5F, (byte)0x97, (byte)0x44, (byte)0x17,
            (byte)0xC4, (byte)0xA7, (byte)0x7E, (byte)0x3D, (byte)0x64, (byte)0x5D, (byte)0x19, (byte)0x73,
            (byte)0x60, (byte)0x81, (byte)0x4F, (byte)0xDC, (byte)0x22, (byte)0x2A, (byte)0x90, (byte)0x88,
            (byte)0x46, (byte)0xEE, (byte)0xB8, (byte)0x14, (byte)0xDE, (byte)0x5E, (byte)0x0B, (byte)0xDB,
            (byte)0xE0, (byte)0x32, (byte)0x3A, (byte)0x0A, (byte)0x49, (byte)0x06, (byte)0x24, (byte)0x5C,
            (byte)0xC2, (byte)0xD3, (byte)0xAC, (byte)0x62, (byte)0x91, (byte)0x95, (byte)0xE4, (byte)0x79,
            (byte)0xE7, (byte)0xC8, (byte)0x37, (byte)0x6D, (byte)0x8D, (byte)0xD5, (byte)0x4E, (byte)0xA9,
            (byte)0x6C, (byte)0x56, (byte)0xF4, (byte)0xEA, (byte)0x65, (byte)0x7A, (byte)0xAE, (byte)0x08,
            (byte)0xBA, (byte)0x78, (byte)0x25, (byte)0x2E, (byte)0x1C, (byte)0xA6, (byte)0xB4, (byte)0xC6,
            (byte)0xE8, (byte)0xDD, (byte)0x74, (byte)0x1F, (byte)0x4B, (byte)0xBD, (byte)0x8B, (byte)0x8A,
            (byte)0x70, (byte)0x3E, (byte)0xB5, (byte)0x66, (byte)0x48, (byte)0x03, (byte)0xF6, (byte)0x0E,
            (byte)0x61, (byte)0x35, (byte)0x57, (byte)0xB9, (byte)0x86, (byte)0xC1, (byte)0x1D, (byte)0x9E,
            (byte)0xE1, (byte)0xF8, (byte)0x98, (byte)0x11, (byte)0x69, (byte)0xD9, (byte)0x8E, (byte)0x94,
            (byte)0x9B, (byte)0x1E, (byte)0x87, (byte)0xE9, (byte)0xCE, (byte)0x55, (byte)0x28, (byte)0xDF,
            (byte)0x8C, (byte)0xA1, (byte)0x89, (byte)0x0D, (byte)0xBF, (byte)0xE6, (byte)0x42, (byte)0x68,
            (byte)0x41, (byte)0x99, (byte)0x2D, (byte)0x0F, (byte)0xB0, (byte)0x54, (byte)0xBB, (byte)0x16};

    public byte[] Inv_SBox;

    public void init_Inv_SBox() {
        Inv_SBox = new byte[SBox.length];
        for(int i =0; i <SBox.length; i++) {
            Inv_SBox[Byte.toUnsignedInt(SBox[i])] = (byte) i;
        }
    }

    byte gmul(byte a1, byte b1) {
        int a = Byte.toUnsignedInt(a1);
        int b = Byte.toUnsignedInt(b1);
        int p = 0;
        int hi_bit_set;
        for(int i = 0; i < 8; i++) {
            if((b & 1) == 1) p ^= a;
            hi_bit_set =  (a & 0x80);
            a <<= 1;
            if(hi_bit_set == 0x80) a ^= 0x1b;
            b >>= 1;
        }
        return (byte) (p & 0xFF);
    }

    public byte getStateAt(int line, int column) {
        return State[column * 4 + line];
    }

    public void setStateAt(int line, int column, byte b) {
        State[column * 4 + line] = b;
    }

    public void shift(int line) {
        byte temp = getStateAt(line, 0);
        for(int column = 0; column < Nk-1; column++) {
            setStateAt(line, column, getStateAt(line, column+1));
        }
        setStateAt(line, Nk-1, temp);
    }

    public byte xor(byte b1, byte b2) {
        return (byte) (Byte.toUnsignedInt(b1) ^ Byte.toUnsignedInt(b2));
    }

    public byte[] XOR(byte[] bytes1, byte[] bytes2) {
        assert(bytes1.length == bytes2.length);
        byte[] resultat = new byte[bytes1.length];
        for(int i =0; i < resultat.length; i++) {
            resultat[i] = xor(bytes1[i], bytes2[i]);
        }
        return resultat;
    }

    public byte xor4(byte b1, byte b2, byte b3, byte b4) {
        return (byte) (Byte.toUnsignedInt(b1) ^ Byte.toUnsignedInt(b2) ^ Byte.toUnsignedInt(b3) ^ Byte.toUnsignedInt(b4));
    }

    public void SubBytes(){
        for(int i = 0; i < State.length; i++){
            State[i] = SBox[Byte.toUnsignedInt(State[i])];
        }
    }

    public void Inv_SubBytes() {
        for(int i = 0; i < State.length; i++){
            State[i] = Inv_SBox[Byte.toUnsignedInt(State[i])];
        }
    }

    public void ShiftRows(){
        for(int line = 1; line < 4; line ++){
            for(int i = 0; i < line; i++) {
                shift(line);
            }
        }
    }
    public void Inv_ShiftRows(){
        for(int line = 1; line < 4; line ++){
            for(int i = 4; i > line; i--) {
                shift(line);
            }
        }
    }

    public void MixColumns(){
        for(int column = 0; column < Nk; column ++) {
            byte a0 = getStateAt(0, column);
            byte a1 = getStateAt(1, column);
            byte a2 = getStateAt(2, column);
            byte a3 = getStateAt(3, column);

            byte b0 = xor4(gmul((byte)0x02, a0), gmul((byte)0x03, a1), gmul((byte)0x01, a2), gmul((byte)0x01, a3));
            byte b1 = xor4(gmul((byte)0x01, a0), gmul((byte)0x02, a1), gmul((byte)0x03, a2), gmul((byte)0x01, a3));
            byte b2 = xor4(gmul((byte)0x01, a0), gmul((byte)0x01, a1), gmul((byte)0x02, a2), gmul((byte)0x03, a3));
            byte b3 = xor4(gmul((byte)0x03, a0), gmul((byte)0x01, a1), gmul((byte)0x01, a2), gmul((byte)0x02, a3));

            setStateAt(0, column, b0);
            setStateAt(1, column, b1);
            setStateAt(2, column, b2);
            setStateAt(3, column, b3);
        }
    }

    public void Inv_MixColumns(){
        for(int column = 0; column < Nk; column ++) {
            byte a0 = getStateAt(0, column);
            byte a1 = getStateAt(1, column);
            byte a2 = getStateAt(2, column);
            byte a3 = getStateAt(3, column);

            byte b0 = xor4(gmul((byte)0x0E, a0), gmul((byte)0x0B, a1), gmul((byte)0x0D, a2), gmul((byte)0x09, a3));
            byte b1 = xor4(gmul((byte)0x09, a0), gmul((byte)0x0E, a1), gmul((byte)0x0B, a2), gmul((byte)0x0D, a3));
            byte b2 = xor4(gmul((byte)0x0D, a0), gmul((byte)0x09, a1), gmul((byte)0x0E, a2), gmul((byte)0x0B, a3));
            byte b3 = xor4(gmul((byte)0x0B, a0), gmul((byte)0x0D, a1), gmul((byte)0x09, a2), gmul((byte)0x0E, a3));

            setStateAt(0, column, b0);
            setStateAt(1, column, b1);
            setStateAt(2, column, b2);
            setStateAt(3, column, b3);
        }
    }

    public void AddRoundKey(int r){
        for(int i=0; i < State.length; i++) {
            State[i] = xor(W[i + r*4*Nk], State[i]);
        }
    }
}