// -*- coding: utf-8 -*-

import java.math.BigInteger;
import java.util.Random;

public class RSA_PKCS1 {

    BigInteger n, e, d;

    public byte[] encrypt(byte[] m) {
        assert(n != null);
        assert(e != null);

        //  Du message m à l'entier représentatif x
        assert(m.length <= 125);

        Random rd = new Random();

        byte[] PS = new byte[128 - 3 - m.length];
        //SecureRandom.getInstanceStrong().nextBytes(PS);
        rd.nextBytes(PS);

        for (int i = 0; i < PS.length; ++i) {
            while (PS[i] == 0x00) {
                PS[i] = (byte) rd.nextInt();
            }
        }

        int i = 0;
        byte[] em = new byte[128];
        em[i++] = 0x00;
        em[i++] = 0x02;
        System.arraycopy(PS, 0, em, i, PS.length); i += PS.length;
        em[i++] = 0x00;
        System.arraycopy(m, 0, em, i, m.length); i += m.length;

        BigInteger x = new BigInteger(1, em);

        System.out.println();
        System.out.println("x = " +  x  + " (en décimal)");
        System.out.println("x = 0x" + String.format("%X", x) + " (en hexadécimal)");

        //  Chiffrement de l'entier représentatif
        BigInteger c = x.modPow(e, n);
        System.out.println("x^e mod n = " + c + " ("+c.bitLength()+" bits)");

        //  Décodage de l'entier représentatif
        byte[] chiffré = c.toByteArray();
        System.out.println("Message chiffré    : " + toHex(chiffré) );
        System.out.println();

        return chiffré;
    }

    public byte[] decrypt(byte[] data) {
        assert(d != null);
        assert(n != null);

        byte[] déchiffré = new BigInteger(data).modPow(d, n).toByteArray();

        int i = 0;
        while (déchiffré[i] != 0x00) ++i;
        i += 1;

        byte[] message = new byte[déchiffré.length - i];
        System.arraycopy(déchiffré, i, message, 0, message.length);

        return message;
    }

    RSA_PKCS1(BigInteger n, BigInteger e, BigInteger d) {
        this.n = n;
        this.e = e;
        this.d = d;
    }

    public static String toHex(byte[] données) {
        StringBuffer sb = new StringBuffer();
        for(byte k: données) sb.append(String.format("0x%02X ", k));
        sb.append(" (" + données.length + " octets)");
        return sb.toString();
    }
}