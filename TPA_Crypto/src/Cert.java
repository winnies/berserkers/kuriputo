import java.io.*;
import java.security.*;

public class Cert {
    public static void main(String[] args) throws Exception {
        byte[] clef = Util.MD5("Alain Turin".getBytes());
        String resume = Util.bytesVersHexaString(Util.resumeEmail("email1.txt", new SimpleHMAC(), clef));
        System.out.println(resume);
        Util.insererXAuth("email1.txt", "email1-auth.txt", resume);
    }
}

