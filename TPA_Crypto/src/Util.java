import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Util {
    // Xor bit à bit entre deux tableaux d'octets
    public static byte[] XOR(byte[] bytes1, byte[] bytes2) {
        assert(bytes1.length == 64 && bytes2.length == 64);
        byte[] resultat = new byte[64];
        for(int i =0; i < 64; i++) {
            resultat[i] = (byte) ((int) bytes1[i] ^ (int) bytes2[i]);
        }
        return resultat;
    }

    public static byte[] MD5(byte[]... bytes) {
        try {
            MessageDigest hacheur = MessageDigest.getInstance("MD5");
            for (byte[] b : bytes)
                hacheur.update(b);
            return hacheur.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Renvoie une chaine de caractères des octets au format hexadécimal
    public static String bytesVersHexaString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for(byte b : bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    public static String emailBody(String emailFileName) throws IOException {
        // Ajout du corps du fichier dans le hâcheur
        File fichier = new File(emailFileName);

        FileReader fr = new FileReader(fichier);
        BufferedReader br = new BufferedReader(fr, 1);

        StringBuilder body = new StringBuilder();

        String currentLine;

        while(!(currentLine = br.readLine()).isBlank());

        while((currentLine = br.readLine()) != null) {
            body.append(currentLine + "\r\n");
        }

        br.close();

        return body.toString();
    }

    public static byte[] resumeEmail(String emailFileName, MAC mac, byte[] clef) throws IOException {
        return mac.generate(emailBody(emailFileName).getBytes(), clef);
    }

    public static boolean verifieEmail(String emailFileName, MAC mac, byte[] clef) throws IOException {
        return bytesVersHexaString(resumeEmail(emailFileName, mac, clef)).equals(emailXAuth(emailFileName));
    }

    // Insère la ligne X-AUTH dans le mail
    public static void insererXAuth(String nomFichier, String nomFichierSortie, String clef) throws Exception {
        File fichier = new File(nomFichier);

        FileReader fr = new FileReader(fichier);
        FileWriter fw = new FileWriter(nomFichierSortie);

        BufferedReader br = new BufferedReader(fr, 1);
        BufferedWriter bw = new BufferedWriter(fw);

        String currentLine;

        while(!(currentLine = br.readLine()).isBlank()) {
            bw.write(currentLine + "\r\n");
        };
        bw.write("X-AUTH: " + clef + "\r\n");
        bw.write("\r\n");
        while((currentLine = br.readLine()) != null) {
            bw.write(currentLine + "\r\n");
        }

        br.close();
        bw.close();
    }

    public static String emailXAuth(String emailFileName) throws IOException {
        File fichier = new File(emailFileName);

        FileReader fr = new FileReader(fichier);
        BufferedReader br = new BufferedReader(fr, 1);

        String currentLine;

        while(!(currentLine = br.readLine()).isBlank()) {
            if(currentLine.substring(0, 8).equals("X-AUTH: ")) {
                return currentLine.substring(8);
            }
        };

        return null;
    }
}

