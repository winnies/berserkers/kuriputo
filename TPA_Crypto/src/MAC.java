// message authentication code
public interface MAC {
    byte[] generate(byte[] message, byte[] key);
}
