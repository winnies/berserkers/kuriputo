import java.io.*;
import java.security.MessageDigest;
import java.util.Arrays;

public class Cert_RFC {

    public static void main(String[] args) throws Exception {
        byte[] clef = Util.MD5("Alain Turin".getBytes());
        String resume = Util.bytesVersHexaString(Util.resumeEmail("email1.txt", new RFCHMAC(), clef));
        System.out.println(resume);
        Util.insererXAuth("email1.txt", "email1-auth.txt", resume);
    }
}

