import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;

public class SimpleHMAC implements MAC {

    @Override
    public byte[] generate(byte[] message, byte[] key) {
        return Util.MD5(message, Util.bytesVersHexaString(key).getBytes());
    }
}
