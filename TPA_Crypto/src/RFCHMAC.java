import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Collection;

public class RFCHMAC implements MAC {

    @Override
    public byte[] generate(byte[] message, byte[] key) {
        // Initialisation de ipad et opad
        byte[] ipad = new byte[64];
        Arrays.fill(ipad, (byte) 0x36);

        byte[] opad = new byte[64];
        Arrays.fill(opad, (byte) 0x5c);

        // Création de la clef
        byte[] hashedKey = new byte[64];
        Arrays.fill(hashedKey, (byte) 0);

        byte[] toCopy = key.length > 64 ? Util.MD5(key) : key;
        for (int i = 0; i < toCopy.length; ++i) {
            hashedKey[i] = toCopy[i];
        }

        return Util.MD5(Util.XOR(hashedKey, opad), Util.MD5(Util.XOR(hashedKey, ipad), message));
    }
}

